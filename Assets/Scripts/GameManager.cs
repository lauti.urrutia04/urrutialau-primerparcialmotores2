using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static bool gameOver = false;
    public static bool gameWon = false;
    public static bool gamePaused = false;
    public static int coins;

    [SerializeField]
    public static int currentPlayer = 0;

    public List<Player> players = new List<Player>();
    void Start()
    {
        coins = 0;
        Physics.gravity = new Vector3(0, -20, 0);
        gameOver = false;
        gameWon = false;
        gamePaused = false;
        Constraints();
    }

    void Update()
    {
        if (currentPlayer > players.Count)
        {
            currentPlayer = 0;
        }
    }

    private void FixedUpdate()
    {
        Constraints();
    }

    public void Constraints()
    {
        foreach(Player p in players)
        {
            if (p == players[currentPlayer])
            {
                p.rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                p.rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }
        }
    }

    private void CheckWin()
    {
        if (Controller_FinishDoor.playerOnDoor)
        {
            gameWon = true;
        }
    }
}
