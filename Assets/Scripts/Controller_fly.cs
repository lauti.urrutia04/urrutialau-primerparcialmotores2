using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_fly : MonoBehaviour
{
    public GameObject player;
    public static float speed;
    private Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.Find("Player2");
    }

    void Update()
    {
        transform.LookAt(player.transform);
        rb.AddForce(this.transform.forward * speed);
        //transform.Translate(speed * Vector3.forward * Time.deltaTime);
        if (player == null)
        {
            Destroy(gameObject);
        }
    }
}
