using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_jumper : Player
{
    public int baseJumps = 1;
    public int jumps;

    public override void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W) && floored)
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
        else if (Input.GetKeyDown(KeyCode.W) && jumps > 0)
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            jumps--;
        }
    }

    public override void OnCollisionEnter(Collision co)
    {
        if (co.gameObject.CompareTag("Floor"))
        {
            floored = true;
            jumps = baseJumps;
        }
        if (co.gameObject.CompareTag("Danger"))
        {
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);
            GameManager.gameOver = true;
        }
        if (co.gameObject.CompareTag("BlackHole"))
        {
            GameManager.currentPlayer++;
        }
    }
}
