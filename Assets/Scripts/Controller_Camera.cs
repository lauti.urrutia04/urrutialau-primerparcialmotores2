using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera : MonoBehaviour
{
    public List<GameObject> focusPoints;
    private Camera _camera;
    public float damp;
    //public float smooth;
    //public float zoom;
    private Vector3 velocity = Vector3.zero;

    void Start()
    {
        _camera = GetComponent<Camera>();
    }

    
    void LateUpdate()
    {
        Vector3 point = _camera.WorldToViewportPoint(focusPoints[GameManager.currentPlayer].transform.position);
        Vector3 delta = focusPoints[GameManager.currentPlayer].transform.position - _camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
        Vector3 destination = transform.position + delta;
        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, damp);
    }
}
