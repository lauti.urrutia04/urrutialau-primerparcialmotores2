using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_FinishDoor : MonoBehaviour
{
    public static bool playerOnDoor;
    void Start()
    {
        playerOnDoor = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerOnDoor = true;
            GameManager.gameWon = true;
        }
    }
}
