using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int speed = 10;
    public int boost = 5;
    public int jumpForce = 15;

    public int playerNumber;

    public bool floored = true;
    public bool left;
    public bool right;

    public Rigidbody rb;
    internal RaycastHit leftHit, rightHit;

    public static Player _player;

    private void Awake()
    {
        if (_player == null)
        {
            _player = GameObject.FindObjectOfType<Player>();
            if (_player == null ) 
            {
                GameObject container = new GameObject("Player");
                _player = container.AddComponent<Player>();
            }
        }
        else
        {
        }
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public virtual void Update()
    {
        if (GameManager.currentPlayer == playerNumber)
        {
            Jump();
            CheckBorders();
        }
    }
    public virtual void FixedUpdate()
    {
        if (GameManager.currentPlayer == playerNumber)
        {
            GetInput();
        }
        
    }

    public virtual void GetInput()
    {
        if (Input.GetKey(KeyCode.A) && left)
        {
            rb.velocity = new Vector3(1*-speed, rb.velocity.y, 0);
            //if (Input.GetKey(KeyCode.Space))
            //{
            //    rb.AddForce(new Vector3(-boost * 10, 0, 0), ForceMode.Impulse);
            //}
        }
        else if (Input.GetKey(KeyCode.D) && right)
        {
            rb.velocity = new Vector3(1*speed, rb.velocity.y, 0);
            //if (Input.GetKey(KeyCode.Space))
            //{
            //    rb.AddForce(new Vector3(boost * 10, 0, 0), ForceMode.Impulse);
            //}
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }

        
    }
    public virtual void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W) && floored)
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
    }

    public bool CheckLeft()
    {
        Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z), Vector3.left);
        Debug.DrawRay(ray.origin, ray.direction, Color.red);
        return Physics.Raycast(ray, out leftHit, transform.localScale.x / 1.5f);
    }
    public bool CheckRight()
    {
        Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z), Vector3.right);
        Debug.DrawRay(ray.origin, ray.direction, Color.red);
        return Physics.Raycast(ray, out rightHit, transform.localScale.x / 1.8f);
    }
    public virtual void CheckBorders()
    {
        if (CheckLeft())
        {
            left = false;
        }
        else
        {
            left = true;
        }
        if (CheckRight())
        {
            right = false;
        }
        else
        {
            right = true;
        }
    }


    public virtual void OnCollisionEnter(Collision co)
    {
        if (co.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
        if (co.gameObject.CompareTag("Danger"))
        {
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);
            GameManager.gameOver = true;
        }
        if (co.gameObject.CompareTag("BlackHole"))
        {
            GameManager.currentPlayer++;
        }
    }
    public virtual void OnCollisionExit(Collision co)
    {
        if (co.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
}
