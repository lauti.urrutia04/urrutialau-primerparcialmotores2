using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUD_controller : MonoBehaviour
{
    public TMPro.TMP_Text winloseText;
    void Start()
    {
        winloseText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (GameManager.gameOver)
        {
            Time.timeScale = 0;
            winloseText.text = "You Lost";
            winloseText.gameObject.SetActive(true);
        }
        if (GameManager.gameWon)
        {
            Time.timeScale = 0;
            winloseText.text = "You Won!";
            winloseText.gameObject.SetActive(true);
        }
        if (GameManager.gamePaused)
        {
            Time.timeScale = 0;
            winloseText.text = "Game Paused";
            winloseText.gameObject.SetActive(true);
        }
    }
}
