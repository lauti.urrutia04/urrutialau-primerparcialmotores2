using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Monkey : Player
{
    public bool up;
    public bool down;
    public bool movingL;
    public bool movingR;
    public bool movingU;
    public bool movingD;
    public float delay = 0.5f;
    private RaycastHit downHit, upHit;

    public override void CheckBorders() 
    {
        if (movingL)
        {
            left = false;
            right = false;
            up = false;
            down = false;
            if (CheckLeft())
            {
                Invoke("MovLF", delay);
            }
        }
        else if (movingR)
        {
            left = false;
            right = false;
            up = false;
            down = false;
            if (CheckRight())
            {
                Invoke("MovRF", delay);
            }
        }
        else if (movingU)
        {
            left = false;
            right = false;
            up = false;
            down = false;
            if (CheckUp())
            {
                Invoke("MovUF", delay);
            }
        }
        else if (movingD)
        {
            left = false;
            right = false;
            up = false;
            down = false;
            if (CheckDown())
            {
                Invoke("MovDF", delay);
            }
        }
        else
        {
            left = true;
            right = true;
            up = true;
            down = true;
        }
        /*
        else
        {
            if (CheckLeft())
            {
                left = false;
            }
            else
            {
                left = true;
            }
            if (CheckRight())
            {
                right = false;
            }
            else
            {
                right = true;
            }
            if (CheckUp())
            {
                up = false;
            }
            else
            {
                up = true;
            }
            if (CheckDown())
            {
                down = false;
            }
            else
            {
                down = true;
            }
        }
        */
    } 
    private bool CheckUp()
    {
        Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z), Vector3.up);
        Debug.DrawRay(ray.origin, ray.direction, Color.green);
        return Physics.Raycast(ray, out upHit, transform.localScale.y / 1.8f);
    }
    private bool CheckDown()
    {
        Ray ray = new Ray(new Vector3(transform.position.x, transform.position.y, transform.position.z), Vector3.down);
        Debug.DrawRay(ray.origin, ray.direction, Color.green);
        return Physics.Raycast(ray, out upHit, transform.localScale.y / 1.8f);
    }

    //Metodos para ser invocados tras un delay
    private void MovLF()
    {
        movingL = false;
    }
    private void MovRF()
    {
        movingR = false;
    }
    private void MovUF()
    {
        movingU = false;
    }
    private void MovDF()
    {
        movingD = false;
    }

    public override void Update() //le saco el jump()
    {
        if (GameManager.currentPlayer == playerNumber)
        {
            CheckBorders();
        }
    }
    public override void GetInput()
    {
        if (Input.GetKey(KeyCode.A))
        {
            if (left)
            {
                rb.AddForce(new Vector3(-boost, 0, 0));
                movingL = true;
            }

        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (right)
            {
                rb.AddForce(new Vector3(boost, 0, 0));
                movingR = true;
            }

        }
        else if (Input.GetKey(KeyCode.W))
        {
            if (up)
            {
                rb.AddForce(new Vector3(0, boost, 0));
                movingU = true;
            }

        }
        else if (Input.GetKey(KeyCode.S))
        {
            if (down)
            {
                rb.AddForce(new Vector3(0, -boost, 0));
                movingD = true;
            }

        }
    }
}
