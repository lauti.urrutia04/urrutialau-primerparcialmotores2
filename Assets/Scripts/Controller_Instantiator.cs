using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public GameObject fly;
    public GameObject instantiatePos;
    public float respawnTimer = 1;
    public float flySpeed = 10;
    private float time = 0;
    void Start()
    {
        Controller_fly.speed = flySpeed;
    }

    void FixedUpdate()
    {
        if (GameManager.currentPlayer == 2)
        {
            SpawnEnemy();
        }
    }

    private void SpawnEnemy()
    {
        time -= Time.deltaTime;
        if (time <= 0)
        {
            GameObject f;
            f = Instantiate(fly, new Vector3(instantiatePos.transform.position.x, instantiatePos.transform.position.y, 0), Quaternion.identity);
            time = respawnTimer;
            Destroy(f, 10);
        }
    }
}
